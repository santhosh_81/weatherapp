//
//  UIViewControllerExtension.swift
//  weatherapp
//
//  Created by Santhosh Thiyagarajan on 25/2/17.
//  Copyright © 2017 Santhosh Thiyagarajan. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func displayAlert(text: String, completionHandler: ((UIAlertAction) -> Void)?) {
        let myAlert = UIAlertController(title: "Alert", message: text, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: completionHandler)
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
    }
    
}
