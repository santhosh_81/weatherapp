//
//  DateExtension.swift
//  weatherapp
//
//  Created by Santhosh Thiyagarajan on 26/2/17.
//  Copyright © 2017 Santhosh Thiyagarajan. All rights reserved.
//

import Foundation

extension Date
{
    func toLocalStringWithFormat(format: ReadableDateFormat) -> String {
        // change to a readable time format and change to local time zone
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format.rawValue
        dateFormatter.timeZone = NSTimeZone.local
        let timeStamp = dateFormatter.string(from: self)
        return timeStamp
    }
}
