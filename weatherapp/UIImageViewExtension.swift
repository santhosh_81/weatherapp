//
//  UIImageViewExtension.swift
//  weatherapp
//
//  Created by Santhosh Thiyagarajan on 26/2/17.
//  Copyright © 2017 Santhosh Thiyagarajan. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

extension UIImageView {
    
    /// Downlaod image from a url and set to self.
    ///
    /// - parameter url:       URL to download the image from.
    ///
    /// - returns:             Nothing.
    func download(fromUrl url: URL) {
        Alamofire.request(url).validate().responseData { (response) in
            switch response.result {
            case .success:
                guard let data = response.result.value else {
                    return
                }
                let image = UIImage(data: data)
                DispatchQueue.main.async {
                    self.image = image
                }
            case .failure:
                // On failure do nothing
                break
            }
        }
    }
    
}
