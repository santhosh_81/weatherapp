//
//  Weather.swift
//  weatherapp
//
//  Created by Santhosh Thiyagarajan on 25/2/17.
//  Copyright © 2017 Santhosh Thiyagarajan. All rights reserved.
//

import Foundation

struct Weather {
    let temperature: Int
    let minimumTemperature: Int
    let maximumTemperature: Int
    let humidity: Int
    let weatherDescription: String
    let weatherIconCode: String
}
