//
//  ReadableDateFormat.swift
//  weatherapp
//
//  Created by Santhosh Thiyagarajan on 26/2/17.
//  Copyright © 2017 Santhosh Thiyagarajan. All rights reserved.
//

import Foundation

enum ReadableDateFormat: String {
    case EEEE_MMM_d = "EEEE, MMM d"
}
