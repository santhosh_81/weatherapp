//
//  LocationCode.swift
//  weatherapp
//
//  Created by Santhosh Thiyagarajan on 25/2/17.
//  Copyright © 2017 Santhosh Thiyagarajan. All rights reserved.
//

import Foundation

enum LocationCode: Int {
    case Sydney = 2147714
    case Melbourne = 2158177
    case Brisbane = 2174003
    
    static let allValues = [Sydney, Melbourne, Brisbane]
}
