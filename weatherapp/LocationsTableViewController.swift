//
//  LocationsTableViewController.swift
//  weatherapp
//
//  Created by Santhosh Thiyagarajan on 25/2/17.
//  Copyright © 2017 Santhosh Thiyagarajan. All rights reserved.
//

import UIKit
import SVProgressHUD

class LocationsTableViewController: UITableViewController {

    private var locations: [Location] = []
    private var locationSelected: Location? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Generate location list from LocationCode enum
        for eachLocation in LocationCode.allValues {
            let location = Location(locationCode: eachLocation, weather: nil)
            locations.append(location)
        }
        
        // Display activity indicator
        SVProgressHUD.show(withStatus: "Downloading")
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.light)
        
        // Retrieve weather information for each location
        let weatherService = WeatherService()
        for index in 0..<locations.count {
            weatherService.retrieveWeatherInfo(locationCode: locations[index].locationCode, completionHandler: { (weather: Weather?, error: WAError?) in
                guard weather != nil else {
                    SVProgressHUD.dismiss()
                    self.displayAlert(text: error!.localizedDescription, completionHandler: nil)
                    return
                }
                self.locations[index].weather = weather
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    self.tableView.reloadData()
                }
            })
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locations.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Locations"
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.textLabel?.textColor = UIColor.white
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "locationCell", for: indexPath)
        cell.textLabel?.text = "\(locations[indexPath.row].locationCode)"
        guard let weather = locations[indexPath.row].weather else {
            cell.detailTextLabel?.text = "--"
            return cell
        }
        cell.detailTextLabel?.text = "\(weather.temperature)℃"
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        locationSelected = locations[indexPath.row]
        guard locationSelected!.weather != nil else {
            return
        }
        performSegue(withIdentifier: "showLocationDetailVC", sender: self)
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier!{
        case "showLocationDetailVC":
            let locationDetailViewController = segue.destination as! LocationDetailViewController
            locationDetailViewController.locationSelected = locationSelected
            break
        default:
            break
        }
    }

}
