//
//  WeatherService.swift
//  weatherapp
//
//  Created by Santhosh Thiyagarajan on 25/2/17.
//  Copyright © 2017 Santhosh Thiyagarajan. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

typealias WeatherCompletionHandler = (Weather?, WAError?) -> Void

/// Service used to retrieve weather info
struct WeatherService {
    /// OpenWeatherMap app id & url
    private let appId = "1e129bbfd4d931459172739a882d71a1"
    private let url = "http://api.openweathermap.org/data/2.5/weather"
    
    /// Retrieve weather information for a location based on the location code.
    ///
    /// - parameter locationCode:       One of the members of the location code enum.
    /// - parameter completionHandler:  (Weather?, WAError?) -> Void to be called after async operation
    ///
    /// - returns:                      Nothing.
    func retrieveWeatherInfo(locationCode: LocationCode, completionHandler: @escaping WeatherCompletionHandler) {
        let weatherUrl = "\(url)?id=\(locationCode.rawValue)&units=metric&appid=\(appId)"
        Alamofire.request(weatherUrl).validate().responseJSON { response in
            switch response.result {
            case .success:
                let json = JSON(response.result.value!)
                // Get temperatures, weather description and icon and check for parsing error
                guard let temp = json["main"]["temp"].int,
                    let temp_min = json["main"]["temp_min"].int,
                    let temp_max = json["main"]["temp_max"].int,
                    let humidity = json["main"]["humidity"].int,
                    let weather_desp = json["weather"][0]["description"].string,
                    let weather_icon = json["weather"][0]["icon"].string else {
                        completionHandler(nil, WAError.JSONParsingError)
                        return
                }
                let weather = Weather(temperature: temp, minimumTemperature: temp_min, maximumTemperature: temp_max, humidity: humidity,weatherDescription: weather_desp, weatherIconCode: weather_icon)
                completionHandler(weather, nil)
            case .failure(let error):
                completionHandler(nil, WAError.AlamofireServiceError(error))
            }
        }
    }
}
