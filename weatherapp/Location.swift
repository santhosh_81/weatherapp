//
//  Location.swift
//  weatherapp
//
//  Created by Santhosh Thiyagarajan on 25/2/17.
//  Copyright © 2017 Santhosh Thiyagarajan. All rights reserved.
//

import Foundation
import UIKit

struct Location {
    let locationCode: LocationCode
    var weather: Weather?
}
