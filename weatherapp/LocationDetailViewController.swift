//
//  LocationDetailViewController.swift
//  weatherapp
//
//  Created by Santhosh Thiyagarajan on 25/2/17.
//  Copyright © 2017 Santhosh Thiyagarajan. All rights reserved.
//

import UIKit
import QuartzCore

class LocationDetailViewController: UIViewController {
    
    @IBOutlet weak var backgroundImagePlaceholder: UIView!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var weatherSummary: UILabel!
    @IBOutlet weak var minimumTemperature: UILabel!
    @IBOutlet weak var maximumTemperature: UILabel!
    @IBOutlet weak var humidity: UILabel!
    
    var locationSelected: Location!
    var backgroundImageView: UIImageView!
    var weatherConditionImageView: UIImageView!
    var lightningTimer: Timer?
    var lightningImageView: UIImageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set title on the navigation bar to selected location
        navigationItem.title = "\(Date().toLocalStringWithFormat(format: .EEEE_MMM_d))"

        // Set location, summary, temp, min temp, max temp & humidity
        location.text = "\(locationSelected.locationCode)"
        temperature.text = "\(locationSelected.weather!.temperature)°"
        weatherSummary.text = locationSelected.weather!.weatherDescription
        minimumTemperature.text = "\(locationSelected.weather!.minimumTemperature)°"
        maximumTemperature.text = "\(locationSelected.weather!.maximumTemperature)°"
        humidity.text = "\(locationSelected.weather!.humidity)%"
        
        // Set background image
        let imageFileName = "\(locationSelected.locationCode)".lowercased()
        backgroundImageView = UIImageView(image: UIImage(named: imageFileName))
        backgroundImageView.alpha = 0.5
        backgroundImagePlaceholder.addSubview(backgroundImageView)
        
        // Check if weather description contains the string clear
        if "\(locationSelected.weather!.weatherDescription)".contains("clear") {
            // Set weather conditions to sunny by adding sunshine.png image
            weatherConditionImageView = UIImageView(image: UIImage(named: "sunshine"))
            weatherConditionImageView.alpha = 0.5
            backgroundImagePlaceholder.addSubview(weatherConditionImageView)
        }
        
        // Check if weather description contains the string thunderstorm, drizzle, rain
        if "\(locationSelected.weather!.weatherDescription)".contains("thunderstrom")  {
            // Set weather conditions to rain by using CAEmitterLayer
            backgroundImagePlaceholder.generateRainfall(type: .heavy)
            // Generate lightning with an interval of 3 seconds
            lightningImageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: backgroundImagePlaceholder.bounds.size))
            lightningImageView!.contentMode = .scaleAspectFill
            lightningImageView!.alpha = 0.0
            lightningImageView!.image = UIImage(named: "lightning")
            backgroundImagePlaceholder.addSubview(lightningImageView!)
            lightningImageView!.center = CGPoint(x: 100, y: 0)
            lightningTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(popupAndFade), userInfo: nil, repeats: true)
        }
        if "\(locationSelected.weather!.weatherDescription)".contains("rain")  {
            // Set weather conditions to rain by using CAEmitterLayer
            backgroundImagePlaceholder.generateRainfall(type: .medium)
        }
        if "\(locationSelected.weather!.weatherDescription)".contains("drizzle")  {
            // Set weather conditions to rain by using CAEmitterLayer
            backgroundImagePlaceholder.generateRainfall(type: .light)
        }
        
        // Check if weather description contains the string cloud, thunderstorm, drizzle, rain
        if "\(locationSelected.weather!.weatherDescription)".contains("cloud") || "\(locationSelected.weather!.weatherDescription)".contains("thunderstrom") || "\(locationSelected.weather!.weatherDescription)".contains("drizzle") || "\(locationSelected.weather!.weatherDescription)".contains("rain") {
            // Set weather conditions to cloud by adding cloud.png image and animating
            weatherConditionImageView = UIImageView(image: UIImage(named: "cloud"))
            weatherConditionImageView.alpha = 0.5
            backgroundImagePlaceholder.addSubview(weatherConditionImageView)
            weatherConditionImageView.center = CGPoint(x: weatherConditionImageView.bounds.size.width / 2, y: 50)
            UIView.animate(withDuration: 100) {
                let translationX = self.weatherConditionImageView.bounds.width - self.backgroundImagePlaceholder.bounds.width
                self.weatherConditionImageView.center.x -= translationX
            }
        }

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        // Invalidate lightning timer if not nil
        if lightningTimer != nil {
            lightningTimer!.invalidate()
        }
    }
    
    // Convenience method for animating & randomising lightning imageView apperance
    func popupAndFade() {
        lightningImageView!.alpha = 0.5
        // Fade it away and then hide
        UIView.animate(withDuration: 1, animations: { 
            self.lightningImageView!.alpha = 0.0
        }) { (status: Bool) in
            let randomNumber = Float(arc4random_uniform(100))
            self.lightningImageView!.center.x = self.backgroundImagePlaceholder.bounds.width * CGFloat( randomNumber / 100.0)
        }
    }
    
}
