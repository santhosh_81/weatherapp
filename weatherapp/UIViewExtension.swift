//
//  UIViewExtension.swift
//  weatherapp
//
//  Created by Santhosh Thiyagarajan on 27/2/17.
//  Copyright © 2017 Santhosh Thiyagarajan. All rights reserved.
//

import Foundation
import QuartzCore

enum RainfallType {
    case light
    case medium
    case heavy
}

extension UIView {
    
    /// Generate rainfall for a view using CAEmitterLayer
    ///
    /// - parameter type:       Rainfall type, one of light, medium or heavy.
    ///
    /// - returns:              Nothing.
    func generateRainfall(type rainfallType: RainfallType) {
        let emitterLayer = CAEmitterLayer()
        emitterLayer.emitterPosition = CGPoint(x: self.bounds.width / 2, y: 0)
        emitterLayer.emitterZPosition = 5
        emitterLayer.emitterSize = CGSize(width: self.bounds.width, height: 0)
        emitterLayer.emitterShape = kCAEmitterLayerLine
        
        let emitterCell = CAEmitterCell()
        emitterCell.scale = 0.1
        emitterCell.scaleRange = 0.1
        switch rainfallType {
        case .light:
            emitterCell.birthRate = 100
        case .medium:
            emitterCell.birthRate = 250
        case .heavy:
            emitterCell.birthRate = 500
        }
        emitterCell.lifetime = 5.0
        emitterCell.velocity = 200
        emitterCell.velocityRange = 50
        emitterCell.yAcceleration = 250
        emitterCell.contents = UIImage(named: "raindrop")!.cgImage
        
        emitterLayer.emitterCells = [emitterCell]
        self.layer.addSublayer(emitterLayer)
    }
    
}
