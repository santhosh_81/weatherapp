//
//  Error.swift
//  weatherapp
//
//  Created by Santhosh Thiyagarajan on 25/2/17.
//  Copyright © 2017 Santhosh Thiyagarajan. All rights reserved.
//

import Foundation

enum WAError: Error {
    case JSONParsingError
    case AlamofireServiceError(Error)
    
    var localizedDescription: String {
        switch self {
        case .JSONParsingError:
            return "Weather data parsing error"
        case .AlamofireServiceError(let error):
            return error.localizedDescription
        }
    }
}
